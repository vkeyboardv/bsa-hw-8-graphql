const { GraphQLServer } = require('graphql-yoga');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const { prisma } = require('./generated/prisma-client');
const Subscription = require('./resolvers/Subscription')

const resolvers = {
  Query,
  Mutation,
  Subscription
};

const server = new GraphQLServer({
  typeDefs: './server/src/schema.graphql',
  resolvers,
  context: request => ({
    ...request,
    prisma
  })
});

server.start(() => console.log('http://localhost:4000'));
