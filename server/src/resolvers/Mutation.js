function postMessage(parent, args, context) {
  const { text } = args;

  return context.prisma.createMessage({
    text
  });
}

async function postLike(parent, args, context) {
  const { id } = args;
  const where = { id };
  const messageExists = await context.prisma.$exists.message({ id });

  if (messageExists) {
    const likes = await context.prisma.message(where).likes();
    let data = (!likes) ? { likes: 1 } : { likes: likes + 1 };
    return context.prisma.updateMessage({ where, data });
  }

  throw new Error(`Message with that ID does not exist`);
}

async function postDislike(parent, args, context) {
  const { id } = args;
  const where = { id };
  const messageExists = await context.prisma.$exists.message({ id });

  if (messageExists) {
    const dislikes = await context.prisma.message(where).dislikes();
    let data = (!dislikes) ? { dislikes: 1 } : { dislikes: dislikes + 1 };
    return context.prisma.updateMessage({ where, data });
  }

  throw new Error(`Message with that ID does not exist`);
}

module.exports = {
  postMessage,
  postLike,
  postDislike
}
