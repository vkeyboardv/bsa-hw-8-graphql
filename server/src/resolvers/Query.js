async function messages(parent, args, context) {
  const where = args.filter ? { text_contains: args.filter } : {};
  const messages = await context.prisma.messages({
    where,
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy
  });

  return messages;
}

module.exports = {
  messages
}
