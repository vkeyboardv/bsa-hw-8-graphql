import React from 'react';
import Chat from './components/Chat';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <div className="main">
        <Chat />
      </div>
    )
  }
}

export default App;
