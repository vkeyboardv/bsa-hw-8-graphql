import gql from 'graphql-tag';

export const GET_MESSAGES = gql`
  query messageQuery($orderBy: MessagesOrderBy, $filter: String, $first: Int, $skip: Int){
    messages(first: $first, skip: $skip, orderBy: $orderBy, filter: $filter) {
      id text likes dislikes
    }
  }
`;

export const POST_MESSAGE = gql`
  mutation addMessage($text: String!) {
    postMessage(text: $text) {
      id
      text
      likes
    }
  }
`;

export const POST_MESSAGE_LIKE_MUTATION = gql`
  mutation postLikeMutation($id: ID!) {
    postLike(id: $id) {
      id
      text
      likes
    }
  }
`;

export const POST_MESSAGE_DISLIKE_MUTATION = gql`
  mutation postDislikeMutation($id: ID!) {
    postDislike(id: $id) {
      id
      text
      dislikes
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      likes
      dislikes
    }
  }
`;
