import React from 'react'
import { Dimmer, Loader } from 'semantic-ui-react'

class LoadingSpinner extends React.Component {
  render() {
    return (
      <div className="loading-spinner">
        <Dimmer active inverted>
          <Loader inverted>Loading</Loader>
        </Dimmer>
      </div>
    )
  }
}

export default LoadingSpinner;
