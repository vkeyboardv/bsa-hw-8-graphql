import React from 'react';
import Message from '../Message';

class MessageList extends React.Component {
  getMessages() {
    const messagesArray = this.props.messages.map(message => <Message message={message} key={message.id} />);

    return messagesArray;
  }

  render() {
    return (
      <div className="message-list">
        {this.getMessages()}
      </div>
    )
  }
}

export default MessageList;
