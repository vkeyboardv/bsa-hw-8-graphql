import React from 'react';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';
import Header from '../Header';
import { Query } from 'react-apollo';
import { GET_MESSAGES, NEW_MESSAGE_SUBSCRIPTION } from '../../queries';
import LoadingSpinner from '../LoadingSpinner';

import { Input, Pagination } from 'semantic-ui-react'

class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      orderBy: 'createdAt_ASC',
      filter: '',
    };

    this.updateState = this.updateState.bind(this);
  }

  updateState(orderBy) {
    this.setState({ ...this.state, orderBy: orderBy });
  }

  updateInputState(filter) {
    this.setState({ ...this.state, filter: filter })
  }

  _subscribeToNewMessage = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.find(({ id }) => id === newMessage.id);
        if (exists) return prev;

        return { ...prev, messages: [...prev.messages, newMessage] };
      }
    });
  };

  render() {
    const { orderBy, filter } = this.state;

    return (
      <div className="main-app">
        <div className="main-app-filter-input">
          <Input transparent onChange={ev => this.updateInputState(ev.target.value)} size="mini" value={filter} icon='search' placeholder='Search message...' />
        </div>
        <div className="main-app-query">
          <Query query={GET_MESSAGES} variables={{ orderBy, filter }} >
            {({ loading, error, data, subscribeToMore }) => {
              if (loading) return <LoadingSpinner />;
              if (error) return `Error! ${error.message}`;
              this._subscribeToNewMessage(subscribeToMore);
              const { messages } = data;
              const totalPages = Math.ceil(Number(messages.length) / 6);

              return (
                <div className="main-app-chat">
                  <Header count={messages.length} currentState={this.updateState} />
                  <MessageList messages={messages} />
                  <MessageInput />
                  <div className="main-app-pagination">
                    <Pagination
                      boundaryRange={0}
                      defaultActivePage={1}
                      ellipsisItem={null}
                      firstItem={null}
                      lastItem={null}
                      siblingRange={1}
                      totalPages={totalPages} // Pagination is not finished yet
                      size="mini"
                    />
                  </div>
                </div>
              );
            }}
          </Query>
        </div>
      </div>
    )
  }
}

export default Chat;
