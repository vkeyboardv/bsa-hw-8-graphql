import React from 'react';
import { Label, Icon } from 'semantic-ui-react'

class Header extends React.Component {
  handleSort(sortMethod) {
    const { currentState } = this.props;

    currentState(sortMethod);
  }

  render() {
    return (
      <div className="header">
          <Label size="small">
            <Icon name='mail' /> {this.props.count}
          </Label>
          <Label size="small" as="a" onClick={ev => this.handleSort('createdAt_ASC')}>
            <Icon name='angle up' /> MSG
        </Label>
          <Label size="small" as="a" onClick={ev => this.handleSort('createdAt_DESC')}>
            <Icon name='angle down' /> MSG
        </Label>
          <Label size="small" as="a" onClick={ev => this.handleSort('likes_ASC')}>
            <Icon name='angle up' /> LIKES
        </Label>
          <Label size="small" as="a" onClick={ev => this.handleSort('likes_DESC')}>
            <Icon name='angle down' /> LIKES
        </Label>
          <Label size="small" as="a" onClick={ev => this.handleSort('dislikes_ASC')}>
            <Icon name='angle up' /> DISLIKES
        </Label>
          <Label size="small" as="a" onClick={ev => this.handleSort('dislikes_DESC')}>
            <Icon name='angle down' /> DISLIKES
        </Label>
      </div>
    )
  }
}

export default Header;
