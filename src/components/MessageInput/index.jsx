import React from 'react';
import { Mutation } from 'react-apollo';
import { POST_MESSAGE } from '../../queries';

import { Input, Button } from 'semantic-ui-react';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ""
    }
  }

  render() {
    return (
      <div className="message-input-main">
        <Mutation
          mutation={POST_MESSAGE}
          variables={{ text: this.state.text }}
        >
          {addMessage => (
            <div className="message-input-container">
              <Input type='text' placeholder='Type your message...'
                value={this.state.text} onChange={(event) => this.setState({ ...this.state, text: event.target.value })} action>
                <input />
                <Button type='submit' onClick={event => {
                  event.preventDefault();
                  if (!this.state.text.length) return;
                  addMessage();
                  this.setState({ ...this.state, text: "" });
                }}>Send</Button>
              </Input>
            </div>
          )}
        </Mutation>

      </div>
    )
  }
}

export default MessageInput;
