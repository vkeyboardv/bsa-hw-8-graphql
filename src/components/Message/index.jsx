import React from 'react';
import { Mutation } from 'react-apollo';
import { POST_MESSAGE_LIKE_MUTATION, POST_MESSAGE_DISLIKE_MUTATION } from '../../queries';

class Message extends React.Component {

  getThreeDigitId() {
    const messageId = this.props.message.id;

    return messageId.substr(messageId.length - 3);
  }

  render() {
    return (
      <div className="message">
        <div className="message-text">
          {this.props.message.text}
        </div>
        <div className="message-actions">
          <div className="message-info">
            <div className="message-id">ID: {this.getThreeDigitId()}</div>
          </div>
          <Mutation
            mutation={POST_MESSAGE_LIKE_MUTATION}
            variables={{ id: this.props.message.id }}
          >
            {postLikeMutation => (
              <div
                className="message-like"
                onClick={(event) => {
                  event.preventDefault();
                  postLikeMutation();
                }}
              >
                Like [ {this.props.message.likes} ]
              </div>
            )}
          </Mutation>
          <Mutation
            mutation={POST_MESSAGE_DISLIKE_MUTATION}
            variables={{ id: this.props.message.id }}
          >
            {postDislikeMutation => (
              <div
                className="message-dislike"
                onClick={(event) => {
                  event.preventDefault();
                  postDislikeMutation();
                }}
              >
                Dislike [ {this.props.message.dislikes} ]
              </div>
            )}
          </Mutation>
          <div className="message-reply">Reply</div>
        </div>
      </div>
    )
  }
}

export default Message;
